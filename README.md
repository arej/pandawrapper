### Introduction:
A wrapper code made on top of pandamonium (https://github.com/dguest/pandamonium)
-> to create the downloading script for ntuples produced via grid and also a spreadsheet Monitor for status of the jobs if required


### STEPS:

`source setenv.sh`

###### Set your task searches in `makelist.sh`. All running tasks are listed with all details from panda. Be careful with `-d` for last N days of jobs to be searched
`source makelist.sh`
-> `lists/testlist.txt` created

###### (NOT REQUIRED to repeat) Creates a nested dictionary of processes with their corresponding DSIDs - need to have `jsons/json_MC16a.json`? need to change the 
`python MakeMasterJson.py`
-> `jsons/master.json` created

###### Sort the list of tasks from `testlist.txt` and extracts the important parameters, giving `outlist.txt` ready to be used in a dataframe
`python sortlist.py`
-> `lists/outlist.txt` created

###### (OPTIONAL for downloading) Create the monitoring CSV according to input DSIDs from `jsons/master.json`, showing the status of the jobs for the respective DSIDs
`createMonitor.py`
-> `lists/monitor.csv` created

###### (OPTIONAL, only once needed) Create a bash script to create a structure of empty folders according to `master.json`
`make_folders.py`
-> `make_folders.sh` created

###### Create a large set of bash scripts in `downloaders` folder to download all output root files to the corresponding folder of the process
`createDownloader.py`
-> `downloads/*.sh` created

