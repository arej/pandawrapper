import json
import re

with open('jsons/json_MC16a.json') as mc16ajson:
  MC16a_dict = json.load(mc16ajson)

###############################################################################

samples_mc16a_signal_ljets = [
    #'mc16a_TOPQ1_ttgamma_NLO_prod_old',
    #'mc16a_TOPQ1_ttgamma_LO_dec_old',
    #'mc16a_TOPQ1_ttgamma_nominal',
    'mc16a_TOPQ1_ttgamma_NLO_prod',
    'mc16a_TOPQ1_ttgamma_LO_dec',
]

samples_mc16a_ljets = [
    'mc16a_TOPQ1_ttbar_FS',
    'mc16a_TOPQ1_singletop_schan',
    'mc16a_TOPQ1_singletop_Wt_inclusive_DR',
    'mc16a_TOPQ1_Wty_nominal',
    'mc16a_TOPQ1_singletop_tchan',
    'mc16a_TOPQ1_Zgamma',
    'mc16a_TOPQ1_Zjets',
    'mc16a_TOPQ1_Wgamma',
    'mc16a_TOPQ1_Wjets',
    'mc16a_TOPQ1_diboson',
    'mc16a_TOPQ1_ttV',
]

samples_mc16a_modelling_ljets = [
    #'mc16a_TOPQ1_ttgamma_NLO_prod_H7',
    'mc16a_TOPQ1_ttgamma_NLO_prod_var3cUp',
    'mc16a_TOPQ1_ttgamma_NLO_prod_var3cDown',
    'mc16a_TOPQ1_ttgamma_LO_dec_H7',
    #'mc16a_TOPQ1_ttgamma_AFII',
    #'mc16a_TOPQ1_ttgamma_Herwig7',
    #'mc16a_TOPQ1_ttgamma_radiation_up',
    #'mc16a_TOPQ1_ttgamma_radiation_down',
    #'mc16a_TOPQ1_ttbar_AFII',
    #'mc16a_TOPQ1_ttbar_hdamp_var',
    #'mc16a_TOPQ1_ttbar_aMC',
    'mc16a_TOPQ1_ttbar_Herwig7',
    #'mc16a_TOPQ1_singletop_Wt_inclusive_AFII',
    #'mc16a_TOPQ1_singletop_Wt_inclusive_DS',
    #'mc16a_TOPQ1_singletop_Wt_inclusive_MadGraph',
    #'mc16a_TOPQ1_singletop_Wt_inclusive_Herwig7',
    #'mc16a_TOPQ1_Wty_Herwig7',
]

samples_mc16a_signal_dilep = [
    'mc16a_TOPQ1_ttgamma_NLO_prod_old',
    'mc16a_TOPQ1_ttgamma_LO_dec_old',
    'mc16a_TOPQ1_ttgamma_nominal',
    'mc16a_TOPQ1_ttgamma_NLO_prod',
    'mc16a_TOPQ1_ttgamma_LO_dec',
]

samples_mc16a_dilep = [
    'mc16a_TOPQ1_ttbar_dilepton_FS',
    'mc16a_TOPQ1_singletop_schan',
    'mc16a_TOPQ1_singletop_Wt_dilepton_DR',
    'mc16a_TOPQ1_Wty_nominal',
    'mc16a_TOPQ1_singletop_tchan',
    'mc16a_TOPQ1_Zgamma',
    'mc16a_TOPQ1_Zjets',
    'mc16a_TOPQ1_Wgamma',
    'mc16a_TOPQ1_Wjets',
    'mc16a_TOPQ1_diboson',
    'mc16a_TOPQ1_ttV',
]

samples_mc16a_modelling_dilep = [
    'mc16a_TOPQ1_ttgamma_NLO_prod_H7',
    'mc16a_TOPQ1_ttgamma_NLO_prod_var3cUp',
    'mc16a_TOPQ1_ttgamma_NLO_prod_var3cDown',
    'mc16a_TOPQ1_ttgamma_LO_dec_H7',
    'mc16a_TOPQ1_ttgamma_AFII',
    'mc16a_TOPQ1_ttgamma_Herwig7',
    'mc16a_TOPQ1_ttgamma_radiation_up',
    'mc16a_TOPQ1_ttgamma_radiation_down',
    'mc16a_TOPQ1_ttbar_dilepton_AFII',
    'mc16a_TOPQ1_ttbar_dilepton_hdamp_var',
    'mc16a_TOPQ1_ttbar_dilepton_aMC',
    'mc16a_TOPQ1_ttbar_dilepton_Herwig7',
    'mc16a_TOPQ1_singletop_Wt_dilepton_AFII',
    'mc16a_TOPQ1_singletop_Wt_dilepton_DS',
    'mc16a_TOPQ1_singletop_Wt_dilepton_MadGraph',
    'mc16a_TOPQ1_singletop_Wt_dilepton_Herwig7',
    'mc16a_TOPQ1_Wty_Herwig7',
]

###############################################################################
"""
if datatype == 'data':
  if campaign == 'a':
    Samples(samples_data15))
    Samples(samples_data16))
  elif campaign == 'd':
    Samples(samples_data17))
  elif campaign == 'e':
    Samples(samples_data18))

elif datatype == 'truth':
  if campaign == 'a' and channel=='ljets':
    Samples(samples_mc16a_signal_ljets))
  elif campaign == 'a' and channel=='dilep_CR':
    Samples(samples_mc16a_signal_dilep))
  elif campaign == 'd' and channel=='ljets':
    Samples(samples_mc16d_signal_ljets))
  elif campaign == 'd' and channel=='dilep_CR':
    Samples(samples_mc16d_signal_dilep))
  elif campaign == 'e' and channel=='ljets':
    Samples(samples_mc16e_signal_ljets))
  elif campaign == 'e' and channel=='dilep_CR':
    Samples(samples_mc16e_signal_dilep))

elif datatype == 'reco':
  if campaign == 'a' and channel=='ljets':
    Samples(samples_mc16a_ljets))
    Samples(samples_mc16a_modelling_ljets))
  elif campaign == 'a' and channel=='dilep_CR':
    Samples(samples_mc16a_dilep))
    Samples(samples_mc16a_modelling_dilep))

  elif campaign == 'd' and channel=='ljets':
    Samples(samples_mc16d_ljets))
    Samples(samples_mc16d_modelling_ljets))
  elif campaign == 'd' and channel=='dilep_CR':
    Samples(samples_mc16d_dilep))
    Samples(samples_mc16d_modelling_dilep))

  elif campaign == 'e' and channel=='ljets':
    Samples(samples_mc16e_ljets))
    Samples(samples_mc16e_modelling_ljets))
  elif campaign == 'e' and channel=='dilep_CR':
    Samples(samples_mc16e_dilep))
    Samples(samples_mc16e_modelling_dilep))
"""

masterjson = {}

masterjson["reco"] = {}

masterjson["reco"]["ljets"] = {}
for processes in [samples_mc16a_ljets, samples_mc16a_modelling_ljets]:
    for process in processes:
        list_ = MC16a_dict.get(process)
        dsid_ = []
        for listy in list_:
            if(re.search("_a[0-9]+_", listy)):
                simu_ = "AF"
            elif(re.search("_s[0-9]+_", listy)):
                simu_ = "FS"
            else:
                simu_ = "Data"
            dsid_.append(re.sub("mc16\_13TeV\.", "", re.search("mc16\_13TeV\.\w+", listy).group(0)) + '_' + simu_)
        procname = re.sub("mc16a_TOPQ1_", "", process)
        masterjson["reco"]["ljets"][procname] = dsid_
"""
masterjson["reco"]["dilep"] = {}
for processes in [samples_mc16a_dilep, samples_mc16a_modelling_dilep]:
    for process in processes:
        list_ = MC16a_dict.get(process)
        dsid_ = []
        for listy in list_:
            if(re.search("_a[0-9]+_", listy)):
                simu_ = "AF"
            elif(re.search("_s[0-9]+_", listy)):
                simu_ = "FS"
            else:
                simu_ = "Data"
            dsid_.append(re.sub("mc16\_13TeV\.", "", re.search("mc16\_13TeV\.\w+", listy).group(0)) + '_' + simu_)
        procname = re.sub("mc16a_TOPQ1_", "", process)
        masterjson["reco"]["dilep"][procname] = dsid_
"""

masterjson["truth"] = {}

masterjson["truth"]["ljets"] = {}
for process in samples_mc16a_signal_ljets:
    list_ = MC16a_dict.get(process)
    dsid_ = []
    for listy in list_:
        if(re.search("_a[0-9]+_", listy)):
            simu_ = "AF"
        elif(re.search("_s[0-9]+_", listy)):
            simu_ = "FS"
        else:
            simu_ = "Data"
        dsid_.append(re.sub("mc16\_13TeV\.", "", re.search("mc16\_13TeV\.\w+", listy).group(0)) + '_' + simu_)
    procname = re.sub("mc16a_TOPQ1_", "", process)
    masterjson["truth"]["ljets"][procname] = dsid_
"""
masterjson["truth"]["dilep"] = {}
for process in samples_mc16a_signal_dilep:
    list_ = MC16a_dict.get(process)
    dsid_ = []
    for listy in list_:
        if(re.search("_a[0-9]+_", listy)):
            simu_ = "AF"
        elif(re.search("_s[0-9]+_", listy)):
            simu_ = "FS"
        else:
            simu_ = "Data"
        dsid_.append(re.sub("mc16\_13TeV\.", "", re.search("mc16\_13TeV\.\w+", listy).group(0)) + '_' + simu_)
    procname = re.sub("mc16a_TOPQ1_", "", process)
    masterjson["truth"]["dilep"][procname] = dsid_
"""

master_dict = json.dumps(masterjson)
f = open("jsons/master.json","w")
f.write(master_dict)
f.close()



print(masterjson)
