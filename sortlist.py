import re

cutoff_day = 21 # 23rd December
#cutoff_month = 12 

with open('lists/testlist.txt') as f:
  content = f.readlines()

print(len(content))

lineparts = []
for i in range(0, len(content)):
  lineparts.append(content[i].split())

counter = []
user = []
campaign = []
dsid = []
date = []
simu = []
status = []
taskid = []
finish = []
fail = []
outDS = []

### job dependent variables: type: data/reco/truth, channel: ljets,dilep,CR,CR_topo -> depends now on `user`
datatype = []
channel = []

# exception list for arej: dilepton
#excepdilep = ['410472', '410648', '410649', '412003', '410465', '410656', '410657', '410648', '410649', '410558', '411038', '411039', '410482']
#doubledilep = ['410648', '410649']
truthlist = ['412112', '412114', '410389', '504554', '500800']
truthrecolist = ['410389']
#################################
counter_ = 0

for line in lineparts:

  tarikh = re.search("[0-9]{4}-[0-9]{2}-[0-9]{2}", line[-1]).group(0)
  tarikh_m = re.sub("-", "", re.search("-[0-9]{2}-", tarikh).group(0))
  tarikh_minus_m = re.sub(tarikh_m, "", tarikh)
  tarikh_d = re.sub("-", "", re.search("-[0-9]{2}", tarikh_minus_m).group(0))
  #if(not((int(tarikh_m) >= cutoff_month) and (int(tarikh_d) >= cutoff_day))):
  if(int(tarikh_d) == cutoff_day):
    continue
    
  counter_ += 1

  user_ = re.sub("user\.", "", re.search("user\.[a-z]+", line[-1]).group(0))

  if(re.search("_a[0-9]+_", line[-1])):
    simu_="AF"
  elif(re.search("_s[0-9]+_", line[-1])):
    simu_="FS"
  else:
    simu_="Data"

  dsid_ = re.sub("user\.[a-z]+\.", "", re.search("user\.[a-z]+\.\w+", line[-1]).group(0))

  ### job dependent searches
  if(user_=='arej'):
    datatype_ = "reco"
    ch_ = re.search("\_dilep\_", line[-1])
    #if(ch_ and (dsid_ in doubledilep)):
    #  continue
    #elif(ch_ or (dsid_ in excepdilep)):
    if ch_:
      ch = "dilep"
    else:
      ch = "ljets"
    if dsid_ in truthlist and not(dsid_ in truthrecolist and simu_=="AF"):
      datatype_ = "truth"
  """
  else:
    ch = ''
    if(int(tarikh_d)==8):
      ch_ = re.sub("\/", "", re.sub("\_", "", re.search("\_[0-9]\/", line[-1]).group(0)))
      if(int(ch_) == 0):
        ch = "CR"
      elif(int(ch_) == 1):
        ch = "CR_topo"
      elif(int(ch_) == 2 and simu_ == 'Data'):
        ch = "dilep"
      elif((int(ch_) == 3 and simu_ == 'Data') or (int(ch_) == 2 and (simu_ == 'AF' or simu_ == 'FS'))):
        ch = "ljets"

    elif(int(tarikh_d)>=15):
      ch_ = re.search("[0-9]\_[a-z]+\_[0-9]\/", line[-1], re.I)
      if(not ch_):
        ch_ = re.search("[0-9]\_CR_topo\_[0-9]\/", line[-1], re.I)
      ch = re.sub("[0-9]", "", re.sub("\/", "", re.sub("\_","", ch_.group(0))))
      if(ch == "CRtopo"):
        ch = "CR_topo"

    if(ch == "ljets" or ch == "dilep"):
      datatype_ = "truth"
    elif(ch == "CR" or ch == "CR_topo"):
      datatype_ = "reco"
  """
  if(simu_ == "Data"):
      datatype_ = "data"
  datatype.append(datatype_)
  channel.append(ch)
  ##########################


  camp_ = re.search("\_r[0-9]*?\_", line[-1])
  if(not camp_):
    camp_ = re.search("grp[0-9]*?\_", line[-1])
  camp = re.sub("_", "", camp_.group(0))
  if(camp=="r9364"):
    campaign.append("mc16a")
  elif(camp=="r10201"):
    campaign.append("mc16d")
  elif(camp=="r10724"):
    campaign.append("mc16e")
  elif(camp=="grp15" or camp=="grp16" or camp=="grp17" or camp=="grp18"):
    campaign.append(camp)


  simu.append(simu_)
  status.append(line[0])
  taskid.append(line[1])
  finish.append(line[3])
  fail.append(line[4])
  date.append(tarikh)
  counter.append(counter_)
  outDS.append(re.sub("\/", "_*root", line[-1]))
  user.append(user_)
  dsid.append(dsid_)

outlist = open("lists/outlist.txt",'w')

for i in range(len(counter)):
  outlist.write(str(counter[i]) +' '+ str(dsid[i]) +' '+ str(simu[i]) +' '+ str(datatype[i]) +' '+ str(channel[i]) +' '+ str(campaign[i]) +' '+ str(status[i]) +' '+ str(finish[i]) +' '+ str(fail[i]) +' '+ str(taskid[i]) +' '+ str(user[i]) +' '+ str(outDS[i]) +'\n')

outlist.close()







#print(channel)
#print(user)
#print(campaign)
print(len(dsid))
print(len(date))
print(len(outDS))
print(len(counter))
#print(simu)
#print(status, taskid, finish, fail)
print(len(channel))
#print(datatype)

