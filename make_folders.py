import json
import re

with open('jsons/master.json') as masterjson:
  master_dict = json.load(masterjson)


printer = open("make_folders.sh",'w') 


channels = ['ljets']#, 'dilep', 'CR', 'CR_topo']
truth_ch = ['ljets']#, 'dilep']
all_channels = [channels, truth_ch]
datacamps = ['grp15', 'grp16', 'grp17', 'grp18']
chorigin = {'ljets': 'ljets'}#, 'dilep': 'dilep', 'CR': 'ljets', 'CR_topo': 'ljets'}

for ch in channels:
	printer.write('mkdir '+ch+';\n')

for i, datatype in enumerate(master_dict.keys()):
	print(i, datatype)
	for ch in all_channels[i]:
		chan_dict = master_dict.get(datatype).get(chorigin.get(ch))

		for proc in chan_dict.keys():
			printer.write('mkdir '+ch+'/'+proc+'/ \n')
		if(i==1):
			printer.write('mkdir '+ch+'/data/ \n')

