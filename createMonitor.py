import json
import re
import pandas as pd

with open('lists/outlist.txt') as sortedlist:
  content = sortedlist.readlines()

print("inputlist size:", len(content))

lineparts = []
for i in range(len(content)):
  lineparts.append(content[i].split())

df = pd.DataFrame(lineparts, columns = ['counter', 'dsid', 'simu', 'datatype', 'channel', 'campaign', 'status', 'finish', 'fail', 'taskid', 'user', 'outDS'])

with open('jsons/master.json') as masterjson:
  master_dict = json.load(masterjson)


channels = ['ljets', 'dilep']#, 'CR', 'CR_topo']
truth_ch = ['ljets', 'dilep']
all_channels = [channels, truth_ch]
campaigns = ['mc16a', 'mc16d', 'mc16e']
datacamps = ['grp15', 'grp16', 'grp17', 'grp18']
chorigin = {'ljets': 'ljets', 'dilep': 'dilep'}#, 'CR': 'ljets', 'CR_topo': 'ljets'}

printer = open("lists/monitor.csv",'w') # the list of all DSIDs arranged according to master.json in a CSV file to be used for job monitoring in a spreadsheet

for i, datatype in enumerate(master_dict.keys()):
	print(i, datatype)
	printer.write(',,,,,'+datatype+',,,,,,\n')
	for ch in all_channels[i]:
		chan_dict = master_dict.get(datatype).get(chorigin.get(ch))
		print(chan_dict.keys())
		printer.write(',,,,,'+ch+',,,,,\n')
		printer.write(',,,mc16a,,,mc16d,,,mc16e,\n')
		printer.write(',DSID,status,Nfinish %,Nfail %,status,Nfinish %,Nfail %,status,Nfinish %, Nfail %\n')
		for proc in chan_dict.keys():
			printer.write(proc+',,,,,,,,,,'+'\n')
			print(proc+',,,,,,,,,,'+'\n')
			dsids_proc = chan_dict.get(proc)
			for dsid_ in dsids_proc:
				dsid = re.sub("_", "", re.search("[0-9]+\_", dsid_).group(0))
				simu = re.sub("_", "", re.search("\_[a-z]*?\w+", dsid_).group(0))

				print_all = ''
				for camp in campaigns:
					status_ = re.sub('[0-9]+   ', '', df[(df['datatype']==datatype) & (df['channel']==ch) & (df['dsid']==dsid) & (df['campaign']==camp) & (df['simu']==simu)]['status'].to_string())
					if status_ == 'Series([], )':
						status_ = 'bug'

					fin_string = re.sub("\%", "", re.sub('[0-9]+   ', '', df[(df['datatype']==datatype) & (df['channel']==ch) & (df['dsid']==dsid) & (df['campaign']==camp) & (df['simu']==simu)]['finish'].to_string()))
					if fin_string != 'Series([], )':
						fin_ = int(fin_string)
					else:
						print('found bug in finding ',datatype, ch, dsid, camp, simu)
						fin_ = 'bug'
					fail_string = re.sub("\%", "", re.sub('[0-9]+   ', '', df[(df['datatype']==datatype) & (df['channel']==ch) & (df['dsid']==dsid) & (df['campaign']==camp) & (df['simu']==simu)]['fail'].to_string()))
					if fail_string != 'Series([], )':
						fail_ = int(fail_string)
					else:
						print('found bug in finding ',datatype, ch, dsid, camp, simu)
						fail_ = 'bug'
					print_ = ','+str(status_)+','+str(fin_)+','+str(fail_)
					print_all += print_

				printer.write(','+str(dsid)+print_all+','+'\n')

		printer.write('\n\n\n\n\n')


printer.write(',,,,,data,,,,,,\n')
for ch in channels:
	printer.write(',,,,,'+ch+',,,,,\n')
	printer.write(',,,grp15,,,grp16,,,grp17,,,grp18,\n')
	printer.write(',,status,Nfinish %,Nfail %,status,Nfinish %,Nfail %,status,Nfinish %, Nfail %,status,Nfinish %, Nfail %\n')
	print_all = ''
	for camp in datacamps:
		status_ = re.sub('[0-9]+   ', '', df[(df['datatype']=='data') & (df['channel']==ch) & (df['campaign']==camp)]['status'].to_string())
		fin_ = int(re.sub("\%", "", re.sub('[0-9]+   ', '', df[(df['datatype']=='data') & (df['channel']==ch) & (df['campaign']==camp)]['finish'].to_string())))
		fail_ = int(re.sub("\%", "", re.sub('[0-9]+   ', '', df[(df['datatype']=='data') & (df['channel']==ch) & (df['campaign']==camp)]['fail'].to_string())))
		print_ = ','+str(status_)+','+str(fin_)+','+str(fail_)
		print_all += print_
	printer.write(','+print_all+','+'\n')
	printer.write('\n\n\n\n\n')

printer.close()
