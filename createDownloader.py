import json
import re
import pandas as pd

with open('lists/outlist.txt') as sortedlist:
  content = sortedlist.readlines()

print("inputlist size:", len(content))

lineparts = []
for i in range(len(content)):
  lineparts.append(content[i].split())

pd.options.display.max_colwidth = 200
df = pd.DataFrame(lineparts, columns = ['counter', 'dsid', 'simu', 'datatype', 'channel', 'campaign', 'status', 'finish', 'fail', 'taskid', 'user', 'outDS'])

with open('jsons/master.json') as masterjson:
  master_dict = json.load(masterjson)


channels = ['ljets']#, 'dilep', 'CR', 'CR_topo']
truth_ch = ['ljets']#, 'dilep']
all_channels = [channels, truth_ch]
campaigns = ['mc16a', 'mc16d', 'mc16e']
datacamps = ['grp15', 'grp16', 'grp17', 'grp18']
chorigin = {'ljets': 'ljets'}#, 'dilep': 'dilep', 'CR': 'ljets', 'CR_topo': 'ljets'}
chindex = {'ljets': 0}#, 'dilep': 1, 'CR': 2, 'CR_topo': 3}

printer = []
proc_counter = 0


for i, datatype in enumerate(master_dict.keys()):
	#print(i, datatype)
	for ch in all_channels[i]:
		chan_dict = master_dict.get(datatype).get(chorigin.get(ch))

		for proc in chan_dict.keys():
			printer.append( open("downloaders/{}_{}.sh".format(ch, proc),'w') )
			printer[proc_counter].write('cd '+ch+'/'+proc+'/; \n')
			dsids_proc = chan_dict.get(proc)
			for dsid_ in dsids_proc:
				dsid = re.sub("_", "", re.search("[0-9]+\_", dsid_).group(0))
				simu = re.sub("_", "", re.search("\_[a-z]*?\w+", dsid_).group(0))

				for camp in campaigns:
					outDS_ = re.sub('[0-9]+   ', '', df[(df['datatype']==datatype) & (df['channel']==ch) & (df['dsid']==dsid) & (df['campaign']==camp) & (df['simu']==simu)]['outDS'].to_string()) #  & (df['status'] != 'running')
					if(outDS_ == 'Series([], )'):
						print('found bug for ', dsid, camp, ch)
					else:
						printer[proc_counter].write('rucio download ' + outDS_ + '; \n')
			printer[proc_counter].write('cd ../../ \n')
			print('done:' + ch + ' ' + proc)
			proc_counter += 1

# data
for ch in channels:
	printer.append( open("downloaders/{}_data.sh".format(ch),'w') )
	printer[proc_counter].write('cd '+ch+'/data/; \n')
	for camp in datacamps:
		outDS_ = re.sub('[0-9]+   ', '', df[(df['datatype']=='data') & (df['channel']==ch) & (df['campaign']==camp)]['outDS'].to_string()) # & (df['status'] != 'running')
		#if(outDS_ != 'Series([], )'):
		printer[proc_counter].write('rucio download ' + outDS_ + '; \n')
		#print('ok'+ str(proc_counter))
	printer[proc_counter].write('cd ../../ \n')
	proc_counter += 1
	print('done:' + ch + ' data')
